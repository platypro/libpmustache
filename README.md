Platypro's Mustache
===================
A simple mustache parser/generator written in C. To use the library, link "backend/pmustache.c" and "backend/escape.c" into your program along with any data providers from "backend/providers" then add the "backend" directory to your include path.

There is CMake available, using add_subdirectory from the parent project will expose variables ${PMUSTACHE_PROVIDER_WJE_SRCS} and ${PMUSTACHE_INCLUDE_DIRS} as well as library target pmustache.

Within this repository will be a growing collection of providers for various libraries, as well as escape functions for common template types.

There is a test suite for debugging. It is built by enabling option PMUSTACHE_BUILD_TESTSUITE as well as git submodules 'deps/wjelement' and 'deps/spec'.

## Basic usage
This library has both a compile stage and a generation stage.  

The compile stage creates an index based upon a continuous memory buffer. It does this by reading the buffer one byte at a time then creating a linked-list of symbols with a type, source pointer, and length. This is done by calling mustache_mkindex(), which when complete returns a PMUS_TEMPLATE*.  

The generation stage happens whenever the template needs to be rendered. A PMUS_CONTEXT structure must be constructed with a data provider (see Providers), write callbacks, and a template. Calling mustache_generate() with the context along with some data to pass callbacks will generate a template with all variables replaced and sections iterated.  

Once finished, indexes should be destroyed using mustache_destroyIndex().  

## Providers
Providers supply values to template generation. They are found in backend/providers More detailed info on creating these are found in backend/pmustache.h . For now these are built to integrate with existing libraries, but in the future a more complete provider may become available.  
The following providers are available:  
 * WJElement (provider.wje.c) (requires WJElement library with integers distinguished)  

## Features
* [X] Comments  
* [X] Delimiters  
* [X] Inverted/Regular Sections  
* [X] Escape overloading  
* [X] CMake integration  
* [X] Test suite  

## TODO
* [ ] Partials  
* [ ] Change Delimiter  
* [ ] Create index from file  
* [ ] Command-Line frontend  

/* Aeden McClain (c) 2019
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of libpmustache.
 */

#include "pmus-input-stream.h"
#include "pmustache/template.h"

#include <gio/gio.h>
#include <glib.h>

static void
pmus_input_stream_init (PmusInputStream *self);

static gssize
pmus_input_stream_read (GInputStream  *stream,
                        void          *buffer,
                        gsize          count,
                        GCancellable  *cancellable,
                        GError       **error);

static void
pmus_input_stream_dispose (GObject *gobject);

static void
pmus_input_stream_finalize (GObject *gobject);

struct _PmusInputStream
{
  GInputStream parent_instance;
  gboolean finished;

  PMUS_OPERATION* operation;
};

G_DEFINE_TYPE(PmusInputStream, pmus_input_stream, G_TYPE_INPUT_STREAM)

/* 
 * forward definitions
 */

static void
pmus_input_stream_class_init (PmusInputStreamClass *klass)
{
  GInputStreamClass* istream_class;
  GObjectClass* object_class;
  
  object_class = G_OBJECT_CLASS(klass);
  object_class->dispose = pmus_input_stream_dispose;
  object_class->finalize = pmus_input_stream_finalize;
  
  istream_class = G_INPUT_STREAM_CLASS (klass);
  istream_class->read_fn  = pmus_input_stream_read;

}

static void
pmus_input_stream_init (PmusInputStream *self)
{
}

static void
pmus_input_stream_dispose (GObject *gobject)
{
  PmusInputStream* stream = PMUS_INPUT_STREAM(gobject);
  stream->operation = NULL;
  G_OBJECT_CLASS (pmus_input_stream_parent_class)->dispose (gobject);
}

static void
pmus_input_stream_finalize (GObject *gobject)
{
  PmusInputStream* stream = PMUS_INPUT_STREAM(gobject);
  if(stream->operation) mustache_cancel(stream->operation);
  G_OBJECT_CLASS (pmus_input_stream_parent_class)->finalize (gobject);
}

PmusInputStream*
pmus_input_stream_open (PMUS_BUILDER* builder,
                        GError** error)
{
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  PmusInputStream* result = PMUS_INPUT_STREAM(g_object_new(TYPE_PMUS_INPUT_STREAM, NULL));
  
  mustache_cancel(result->operation);
  result->operation = mustache_begin(builder);
  
  return result;
}

static gssize
pmus_input_stream_read (GInputStream  *stream,
                        void          *buffer,
                        gsize          count,
                        GCancellable  *cancellable,
                        GError       **error)
{
  PmusInputStream* astream = PMUS_INPUT_STREAM(stream);
  if(!astream->finished)
  {
    gsize newCount = count;
    astream->finished = mustache_next(astream->operation, buffer, &newCount);
    return (count - newCount);
  } 
  else
  {
    astream->finished = false;
    return 0;
  }
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

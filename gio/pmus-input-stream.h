/* Aeden McClain (c) 2019
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of libpmustache.
 */

/* inclusion guard */
#ifndef __PMUS_INPUT_STREAM_H__
#define __PMUS_INPUT_STREAM_H__

#include <glib-object.h>
#include <pmustache/template.h>
#include <gio/gio.h>

G_BEGIN_DECLS

/*
 * Type declaration.
 */
#define TYPE_PMUS_INPUT_STREAM pmus_input_stream_get_type ()
G_DECLARE_FINAL_TYPE (PmusInputStream, pmus_input_stream, PMUS, INPUT_STREAM, GInputStream)

/*
 * Method definitions.
 */
PmusInputStream *pmus_input_stream_open (PMUS_BUILDER* builder,
                                         GError** error);

void pmus_input_stream_free(PmusInputStream* stream);

G_END_DECLS

#endif /* __VIEWER_FILE_H__ */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

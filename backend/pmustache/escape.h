/* Aeden McClain (c) 2019
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of libpmustache.
 */

#ifndef INCLUDE_PM_ESCAPE_H
#define INCLUDE_PM_ESCAPE_H

#include <stdio.h>

/** Escape callback
 *  \param dest      Destination string
 *  \param destLeft  Max amount of destination string to process
 *  \param src       Source string
 *  \param srcLeft   Max amount of source string to process
 *  \returns      The number of bytes actally processed from src
 */
typedef size_t (*PMUS_ESCAPECB) (char* dest, size_t* destLeft, char* src, size_t* srcLeft);

/** XML/HTML/SVG escape callback (Used as the default) */
extern size_t pmus_escape_xml(char* dest, size_t* destLeft, char* src, size_t* srcLeft);

/** Simple copy callback */
extern size_t pmus_escape_copy(char* dest, size_t* destLeft, char* src, size_t* srcLeft);

#endif /* INCLUDE_PM_ESCAPE_H */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/* Aeden McClain (c) 2019
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of libpmustache.
 */

#ifndef INCLUDE_PROVIDER_WJE_H
#define INCLUDE_PROVIDER_WJE_H

/* WJElement Provider
 * This requires wjelement to be linked into your program 
 * with wjelement.h in your include path. It also requires
 * WJElement to be built with Integers distinguished and
 * WJE_DISTINGUISH_INTEGER_TYPE defined.
 */

#include "pmustache/template.h"
#include <wjelement.h>

/** The WJElement provider */
extern PMUS_PROVIDER WJEMustacheProvider;

/** The type for user data */
typedef WJElement WJEMUSTACHEPROVIDER_T;

#endif /* INCLUDE_PROVIDER_WJE_H */

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

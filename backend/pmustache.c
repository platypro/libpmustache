/* Aeden McClain (c) 2019
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of libpmustache.
 */

#include <stdlib.h>
#include "pmustache/template.h"
#include "pmustache/escape.h"

#include <stdio.h>
#include <string.h>
#ifdef _WIN32
  #include <malloc.h>
  //#define alloca _alloca
#else
  #include <alloca.h>
#endif


// Internal enums

typedef enum ParserState {
  STATE_COPY,
  STATE_TOKEN_OPEN,
  STATE_GETTYPE,
  STATE_TOKEN_CLOSE,
  STATE_NAME,
  
} PARSERSTATE;

typedef enum PMous_Type {
  PMUS_TYPE_NONE     = 0,
  PMUS_TYPE_VERBATIM = '&',
  PMUS_TYPE_SECTION  = '#',
  PMUS_TYPE_INVERTED = '^',
  PMUS_TYPE_PARTIAL  = '>',
  
  // Temporary tokens
  PMUS_TYPE_VERBATIM_ = '{',
  PMUS_TYPE_CHDELIM_  = '=',
  PMUS_TYPE_SECTION_  = '/',
  PMUS_TYPE_COMMENT   = '!'
  
} PMUS_TYPE;

typedef struct PMus_Buffer {
  struct PMus_Buffer* next;
  char* src;
  size_t len;
} PMUS_BUFFER;

typedef struct PMus_Token {
  struct PMus_Token* next;
  struct PMus_Token* child;
  struct PMus_Token* parent;

  PMUS_TYPE type;
  char* src;
  size_t len;
} PMUS_TOKEN;

struct PMus_Template {
  PMUS_BUFFER buff;
  uint32_t maxDepth;
  uint32_t maxNameLen;
  struct PMus_Token* first;
};

struct PMus_Operation {
  PMUS_PROVIDER provider;
  PMUS_ESCAPECB escape;
  PMUS_OBJECT cobj;
  size_t srcAt;

  char* nameBuf;
  
  void** contexts;
  void** baseContext;
  PMUS_TOKEN* tokenAt;
  bool stack;
};

void closeList( PMUS_TOKEN* t, PMUS_TOKEN** last)
{
  if(!*last)
  {
    t->parent->child = NULL;
    printf("Warning! Section %.*s is empty!\n", (int)t->len, t->src);
  }
  else if(*last)
    (*last)->next = NULL;
  (*last) = t->parent;
  t->parent = t->parent->parent; 
}

void truncateSymbol(PMUS_TOKEN* t, char* cmp)
{
  char* c = t->src + t->len - 1;
  while(t->len && strchr(cmp, *c))
  {
    t->len --;
    c--;
  }
}

PMUS_TEMPLATE* mustache_mkIndex(char* template, size_t template_len)
{
  if(!template) return NULL;
  
  PMUS_TEMPLATE* result = calloc(1, sizeof( PMUS_TEMPLATE ));
  PARSERSTATE state = STATE_COPY;
  result->buff.src = template;
  result->maxDepth = 1;
  //Open first token
  PMUS_TOKEN *last = NULL, *t = result->first = calloc(1, sizeof( PMUS_TOKEN ));
  t->src = template;
  t->type = PMUS_TYPE_NONE;

  char* opdel = "{{";
  uint8_t opdel_top = 1;
  
  char* cldel = "}}";
  uint8_t cldel_top = 1;
  
  uint8_t del_at = 0;
  
  uint8_t  allWhitespace = 1;
  uint32_t depth = 1;
  uint16_t lineat = 0;
  uint16_t colat = 0;
  PMUS_TOKEN* lastLine = t;
  
  if(template_len)
    template_len ++;
  
  while(*template)
  {
    char c = (*template);
    
    if(c == '\n')
    {
      lineat++;
      colat = 0;

      if(allWhitespace > 1)
      {
        if(lastLine)
          truncateSymbol(lastLine, "\t ");
        if(allWhitespace == 3 && last)
          truncateSymbol(last, "\r\n\t ");
        if(allWhitespace == 2 && last)
          truncateSymbol(last, "\t ");
        
        while(t->len && strchr("\r\t ", *t->src))
        {
          t->len--;
          t->src++;
        }
        
        t->len--;
        t->src++;
        allWhitespace = 0;
        
        continue;
      }
      lastLine = t;
      allWhitespace = 1;
    }
    
    switch(state)
    {
    case STATE_COPY:
    {
      if(c == *opdel)
      {
        state = STATE_TOKEN_OPEN;
        del_at = 1;
      }
      else
      {
        if(!strchr(" \r\t\n", c))
        {
          allWhitespace = 0;
        }
        t->len++;
      }
      break;
    }
    case STATE_TOKEN_OPEN:
    {
      if(del_at == opdel_top && c == opdel[del_at])
      {
        //Open delimiter detected        
        if(t->len)
        {
          last = t;
          t = calloc(1, sizeof( PMUS_TOKEN ));
          t->parent = last->parent;
          last->next = t;
        }
        t->src = template;
        t->len = 1;
        state = STATE_GETTYPE;
        del_at ++;
      }
      else if(c != opdel[del_at])
      {
        //No mustache!!
        allWhitespace = 0;
        state = STATE_COPY;
        t->len += del_at + 1;
        del_at = 0;
      }
      break;
    }
    case STATE_GETTYPE:
      t->type = c;
      state = STATE_NAME;
      if(t->type == PMUS_TYPE_CHDELIM_
      || t->type == PMUS_TYPE_VERBATIM
      || t->type == PMUS_TYPE_VERBATIM_
      || t->type == PMUS_TYPE_INVERTED
      || t->type == PMUS_TYPE_PARTIAL
      || t->type == PMUS_TYPE_SECTION
      || t->type == PMUS_TYPE_SECTION_ )
      {
        t->src +=2;
        break; // Ignore the first character
      }
      
      t->src ++;
      //Fall through
    case STATE_NAME:
    {
      if(c == *cldel && !(
           (t->type == PMUS_TYPE_VERBATIM_ && t->src[t->len - 2] != '}')
        || (t->type == PMUS_TYPE_CHDELIM_ && t->src[t->len - 2] != '=')))
      {
        state = STATE_TOKEN_CLOSE;
        del_at = 1;
      }
      else t->len++;
      break;
    }
    case STATE_TOKEN_CLOSE:
    {
      if(del_at == cldel_top)
      {
        //Clear padding
        while(t->len && *t->src == ' ')
        { t->src++; t->len--; }
        
        if(t->len > result->maxNameLen) 
          result->maxNameLen = t->len;
        
        truncateSymbol(t, " ");
        
        switch(t->type)
        {
          case PMUS_TYPE_CHDELIM_:
            //TODO: Implement PMOUS_TYPE_CHDELIM_
            printf("(%d:%d) Change delimiter not implemented!\n", lineat, colat);
            t->len = 0;
            break;
          case PMUS_TYPE_PARTIAL:
            //TODO: Implement PMOUS_TYPE_PARTIAL
            printf("(%d:%d) Partials not implemented!\n", lineat, colat);
            t->len = 0;
            break;
          case PMUS_TYPE_COMMENT:
            //Ignore
            if(allWhitespace == 1)
              allWhitespace = 2; //Close line around comment
            t->len = 0;
            break;
          case PMUS_TYPE_SECTION_:
            depth--;
            if(allWhitespace == 1)
              allWhitespace = 3;
            t->len--;
            if(t->parent && t->len == t->parent->len 
            && !memcmp(t->src, t->parent->src, t->len))
            {
              closeList(t, &last);
              last->next = t;
            } else {
              printf("(%d:%d) Error! Section %.*s can't be closed here! Ignoring symbol.\n", lineat, colat, (int)t->len, t->src);
              t->len = 0;
              //Ignore symbol
            }
            break;
          case PMUS_TYPE_INVERTED:
          case PMUS_TYPE_SECTION:
            depth++;
            if(result->maxDepth < depth) result->maxDepth = depth;
            if(allWhitespace == 1)
              allWhitespace = 3;
            last = NULL;
            t->len--;
            t->child = calloc(1, sizeof( PMUS_TOKEN ));
            t->child->parent = t;
            t = t->child;
            break;
          case PMUS_TYPE_VERBATIM_:
            t->type = PMUS_TYPE_VERBATIM;
            t->len --;
            // Fall Through
          case PMUS_TYPE_VERBATIM:
          default:
            t->len--;
            if(t->len)
            {
              last = t;
              t = calloc(1, sizeof( PMUS_TOKEN ));
              t->parent = last->parent;
              last->next = t;
            }
            break;
        }
        t->len = 0;
        t->src = template + 1;
        t->type = PMUS_TYPE_NONE;
        state = STATE_COPY;
      }
      else if(c != cldel[del_at])
      {
        //No mustache!!
        state = STATE_NAME;
        t->len += del_at;
        if(t->type == PMUS_TYPE_VERBATIM_ || t->type == PMUS_TYPE_CHDELIM_ )
          t->len ++;
      }
      del_at ++;
      
      break;        
    }
    }
    
    colat ++;
    template ++;
    template_len --;
    result->buff.len ++;
    if(template_len == 1) break;
  }
  
  if(allWhitespace > 1)
  {
    truncateSymbol(lastLine, "\t ");
  }
  
  //Clean things up
  while(t->parent)
  {
    printf("List %.*s has not been closed! A close has been implied.\n", (int)t->parent->len, t->parent->src);
    closeList(t, &last);
  }  
  
  if(!*t->src)
  {
    if(last) last->next = NULL;
    if(t->parent && t->parent->child == t) t->parent->child = NULL;
    
    free(t);
  }
  
  return result;
}

char* mustache_eatFile(const char* filename)
{
  char* stream = NULL;
  size_t filesize;
  //Load the file
  FILE* filestream = fopen(filename,"r");

  if(filestream)
  {

    //Get the file size
    fseek(filestream, 0, SEEK_END);
    filesize = ftell(filestream);
    fseek(filestream, 0, SEEK_SET);

    //Allocate room for file
    stream = calloc(1, filesize+1);
    
    //Load the file
    if (stream && fread(stream, sizeof(char), filesize, filestream) == 0)
    {
        free(stream);
        stream = NULL;
    } 
    fclose(filestream);
  }

  return stream;
}

bool mustache_destroyToken(PMUS_TOKEN* token)
{
  PMUS_TOKEN* next;
  while(token)
  {
    next = token->next;
    if(token->child)
      mustache_destroyToken(token->child);
    free(token);
    token = next;
  }
  return true;
}

bool mustache_destroyIndex(PMUS_TEMPLATE* template)
{
  bool result = mustache_destroyToken(template->first);
  free(template);
  return result;
}

size_t writeTag(PMUS_ESCAPECB escape, 
              PMUS_OBJECT* val, 
              size_t* srcAt, 
              char* dest, 
              size_t* destLeft)
{
  size_t srcLeft = 0;
  if(!escape) escape = pmus_escape_xml;
  
  switch(val->type)
  {
  case PMUS_TYPE_INTEGER:
  {
    char buff[11];
    snprintf(buff, 11, "%d", val->integerValue);
    srcLeft = strlen(buff) - *srcAt;
    *srcAt += escape(dest, 
           destLeft, 
           buff + *srcAt, 
           &srcLeft);
    break;
  }
  case PMUS_TYPE_STRING:
  {
    srcLeft = strlen(val->stringValue) - *srcAt;
    *srcAt += escape(dest, 
           destLeft, 
           val->stringValue + *srcAt, 
           &srcLeft);
    break;
  }
  case PMUS_TYPE_DECIMAL:
  {
    char buff[21];
    snprintf(buff, 21, "%.10g", val->decimalValue);
    srcLeft = strlen(buff) - *srcAt;
    *srcAt += escape(dest, 
           destLeft, 
           buff + *srcAt, 
           &srcLeft);
    break;
  }
  default: break;
  }


  return srcLeft;
}

extern PMUS_OPERATION* mustache_begin(PMUS_BUILDER* builder)
{
  PMUS_OPERATION* result = calloc(1, sizeof(PMUS_OPERATION) + (builder->template->maxDepth + 1) * sizeof(void*));
  result->baseContext = result->contexts = (void**)(result + 1);
  result->contexts[0] = NULL;
  result->contexts[1] = builder->baseContext;
  
  result->contexts ++;
  result->nameBuf = calloc(1, builder->template->maxNameLen + 1);
  
  result->escape = builder->escape;
  result->tokenAt = builder->template->first;
  memcpy(&result->provider, builder->provider, sizeof(PMUS_PROVIDER));
  
  return result;
}

extern bool mustache_next(
  PMUS_OPERATION* operation, 
  void* buffer,
  size_t* bytes)
{
  if(*bytes == 0 || !buffer) return false;
  while(*bytes)
  {
    size_t oldSrcAt = operation->srcAt;
    // Update names and contexts
    if(!operation->srcAt && operation->tokenAt->type != PMUS_TYPE_NONE)
    {
      memcpy(operation->nameBuf, operation->tokenAt->src, operation->tokenAt->len);
      operation->nameBuf[operation->tokenAt->len] = '\000';

      if(operation->tokenAt->type == PMUS_TYPE_SECTION 
      || operation->tokenAt->type == PMUS_TYPE_INVERTED)
      {
        if(!operation->tokenAt->child)
        {
          //Nothing to do here, skip section
          operation->tokenAt = operation->tokenAt->next;
          continue;
        }

        PMUS_CONTEXT_RESULT valid = CRESULT_INVALID;
        void** ctxAt = operation->contexts;
        while(ctxAt != operation->baseContext)
        {
          if(*ctxAt)
          valid = 
            operation->provider.changeContext(*ctxAt, operation->nameBuf, operation->contexts + 1);

          if(valid == CRESULT_INVALID
          || (valid == CRESULT_NOEXIST && operation->tokenAt->type == PMUS_TYPE_SECTION))
          {
            // Try the next context
            ctxAt --;
          } else break;
        }
        
        if (*ctxAt && (
           (operation->tokenAt->type == PMUS_TYPE_SECTION
            && (valid == CRESULT_TRUTHEY)) 
        || (operation->tokenAt->type == PMUS_TYPE_INVERTED
            && (valid == CRESULT_FALSEY || valid == CRESULT_NOEXIST))))
        {
          operation->contexts++;
          operation->tokenAt = operation->tokenAt->child;
        }
        else
        {
          // Section not found
          if(operation->tokenAt->next)
            operation->tokenAt = operation->tokenAt->next;
          else if(operation->tokenAt->parent)
            operation->tokenAt = operation->tokenAt->parent;
          else goto MUSTACHE_NEXT_SUCCESS;
        } 

        continue;
      }
      else
      {
        void** ctxAt = operation->contexts;
        while(ctxAt != operation->baseContext)
        {
          operation->cobj = 
            operation->provider.getValue(*ctxAt, operation->nameBuf);
          if(operation->cobj.type != PMUS_TYPE_INVALID) break;
          ctxAt --;
        }
      }
    }
    
    size_t srcLeft = 0;
    
    switch(operation->tokenAt->type)
    {
      case PMUS_TYPE_NONE: // No tag
      {
        srcLeft = operation->tokenAt->len - operation->srcAt;
        operation->srcAt += pmus_escape_copy(buffer, 
                         bytes, 
                         operation->tokenAt->src + operation->srcAt, 
                         &srcLeft);
        break;
      }
      case PMUS_TYPE_VERBATIM:
      {
        srcLeft = writeTag(pmus_escape_copy, 
                           &operation->cobj, 
                           &operation->srcAt, 
                           buffer, 
                           bytes);
        break;
      }
      default: // Regular tag
        srcLeft =
        writeTag(operation->escape,
                 &operation->cobj, 
                 &operation->srcAt, 
                 buffer, 
                 bytes);
        break;
    }
    
    if(!srcLeft)
    {
      // Reset operation for next token
      if(operation->tokenAt->next)
        operation->tokenAt = operation->tokenAt->next;
      else if(operation->tokenAt->parent)
      {
        operation->tokenAt = operation->tokenAt->parent;
        if(!*operation->contexts)
        {
          operation->tokenAt = operation->tokenAt->next;
        }
        operation->contexts --;
      }
      else goto MUSTACHE_NEXT_SUCCESS;
      buffer = (void*)(((char*)buffer) + operation->srcAt);
      operation->srcAt = 0;
      if(!operation->tokenAt) goto MUSTACHE_NEXT_SUCCESS;
    }
    if(operation->srcAt != oldSrcAt) break;
  }
  return false;
MUSTACHE_NEXT_SUCCESS:
  mustache_cancel(operation);
  return true;
}

bool mustache_cancel(PMUS_OPERATION* operation)
{
  if(operation && !operation->stack)
  {
    free(operation->nameBuf);
    free(operation);
  }
  return true;
}

bool mustache_generate(
  PMUS_BUILDER* builder,
  PMUS_WRITECB writeCallback,
  void* writerData)
{
  PMUS_OPERATION op = {0};
  bool done = false;
  char buffer[10];
  size_t ctxSize = (builder->template->maxDepth + 1) * sizeof(void*);
  op.baseContext = op.contexts = alloca(ctxSize);
  memset(op.contexts, 0, ctxSize);
  op.contexts[0] = NULL;
  op.contexts[1] = builder->baseContext;
  op.contexts ++;
  op.nameBuf = alloca(builder->template->maxNameLen + 1);
  
  op.tokenAt  = builder->template->first;
  op.escape   = builder->escape;
  op.provider.changeContext = builder->provider->changeContext;
  op.provider.getValue = builder->provider->getValue;
  op.stack = true;
  
  while(!done)
  {
    size_t wr = 10;
    done = mustache_next(&op, buffer, &wr);
    if((10-wr) != writeCallback(buffer, 10 - wr, writerData)) return false;
  }
  
  return true;
}

size_t mustache_writecb_count(char *data, size_t length, void *userdata)
{
  *(size_t*)userdata += length;
  return length;
}

size_t mustache_writecb_blind_copy(char* data, size_t length, void* userdata_)
{
  char** userdata = (char**)userdata_;
  memcpy(*userdata, data, length);
  *userdata += length;
  return length;
}

extern char* mustache_generate_mem(PMUS_BUILDER* builder)
{
  char* result = NULL;
  size_t len = 0;
  mustache_generate(builder, mustache_writecb_count, &len);
  result = malloc(len + 1);
  char* resultptr = result;
  mustache_generate(builder, mustache_writecb_blind_copy, &resultptr);
  result[len] = '\000';
  return result;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/* Aeden McClain (c) 2019
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of libpmustache.
 */

#include "pmustache/escape.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>

size_t pmus_escape_copy(char* dest, size_t* destLeft, char* src, size_t* srcLeft)
{
  size_t result = 0;
  if(*srcLeft <= *destLeft)
  { 
    memcpy(dest, src, *srcLeft);
    result += *srcLeft;
    *destLeft -= *srcLeft;
    *srcLeft = 0;
  } 
  else
  {
    memcpy(dest, src, *destLeft);
    result += *destLeft;
    *srcLeft -= *destLeft;
    *destLeft = 0;
  }
  return result;
}

size_t pmus_escape_xml(char* dest, size_t* destLeft, char* src, size_t* srcLeft)
{
  char* cpy = NULL;
  char cpychar[2] = "X";
  size_t cpylen = 0;
  size_t total = 0;
  while(*src && srcLeft)
  {
    switch(*src)
    {
      case '<':
        cpy = "&lt;";
        cpylen = 4;
        break;
      case '>':
        cpy = "&gt;";
        cpylen = 4;
        break;
      case '"':
        cpy = "&quot;";
        cpylen = 6;
        break;
      case '&':
        cpy = "&amp;";
        cpylen = 5;
        break;
      case '\'':
        cpy = "&apos;";
        cpylen = 6;
        break;
      default:
        *cpychar = *src;
        cpy = cpychar;
        cpylen = 1;
        break;
    }
    if(cpylen <= *destLeft)
    {
      memcpy(dest, cpy, cpylen);
      dest += cpylen;
      (*srcLeft) --;
      total += cpylen;
      *destLeft -= cpylen;
    } else break;
    src++;
  }
  return total;
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

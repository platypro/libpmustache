/* Aeden McClain (c) 2019
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of libpmustache.
 */

#define WJE_DISTINGUISH_INTEGER_TYPE

#include "pmustache/template.h"
#include "pmustache/provider.wje.h"

/* WJElement header should be in include path */
#include <wjelement.h>

PMUS_OBJECT WJEGetValue(void* base, char* name);
PMUS_CONTEXT_RESULT WJEChangeContext(void* base, char* name, void** prev);

PMUS_OBJECT WJEGetValue(void* base, char* name)
{
  PMUS_OBJECT result;
  WJElement e = WJEGet(((WJElement)base), name, NULL);
  if(!e) return (PMUS_OBJECT){.type=PMUS_TYPE_INVALID};
  switch(e->type)
  {
  case WJR_TYPE_INTEGER:
  {
    result.type = PMUS_TYPE_INTEGER;
    result.integerValue = WJEUInt32(e, NULL, WJE_GET, 0);
    break;
  }
  case WJR_TYPE_NUMBER:
  {
    result.type = PMUS_TYPE_DECIMAL;
    result.decimalValue = WJEDouble(e, NULL, WJE_GET, 0);
    break;
  }
  case WJR_TYPE_STRING:
  {
    result.type = PMUS_TYPE_STRING;
    result.stringValue = WJEString(e, NULL, WJE_GET, 0);
    break;
  }
  default:
    result.type = PMUS_TYPE_INVALID;
  }
  return result;
}

PMUS_CONTEXT_RESULT WJEChangeContext(void* base, char* name, void** prev)
{
  WJElement el = WJEGet(base, name, NULL);
  if(!el) return CRESULT_NOEXIST;
  if(el == *prev) return CRESULT_INVALID;
  switch(el->type)
  {
  case WJR_TYPE_ARRAY:
  {
    *prev = WJEGet(el, "[]", *prev);
    return *prev ? CRESULT_TRUTHEY : CRESULT_FALSEY;
  } 
  case WJR_TYPE_OBJECT:
  {
    *prev = el;
    return CRESULT_TRUTHEY;
  }
  case WJR_TYPE_BOOL:
  case WJR_TYPE_TRUE:
  case WJR_TYPE_FALSE:
  {
    return WJEBool(el, NULL, WJE_GET, FALSE) ? CRESULT_TRUTHEY : CRESULT_FALSEY;
  }
  default:
  {
    *prev = NULL;
    return CRESULT_INVALID;
  }
  }
}

PMUS_PROVIDER WJEMustacheProvider = {.getValue=WJEGetValue, .changeContext=WJEChangeContext};

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/* Aeden McClain (c) 2019
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * License info at bottom.
 *
 * This file is a part of libpmustache.
 */

#include "pmustache/template.h"
#include "pmustache/provider.wje.h"

#define WJE_DISTINGUISH_INTEGER_TYPE

#include <stdio.h>
#include <string.h>
#include <libgen.h>
#include <wjelement.h>
#include <wjreader.h>
#include <unistd.h>

size_t writeTest(char *data, size_t length, void *userdata)
{
  size_t origlength = length;
  printf("Data:\n%.*s\nMatch:\n%.*s\n", (int)length, data, (int)length, *(char**)userdata);
  while(length)
  {
    char s = **(char**)userdata;
    if(!s || s != *data)
      return 0;
    else 
      (*(char**)userdata) ++;
    length--;
    data++;
  }
  return origlength;
}

int main(int argc, char** argv)
{
  enum APIMode
  {
    API_MODE_WRITER,
    API_MODE_POLL,
  };

  enum APIMode amode = API_MODE_WRITER;
  char* testName = NULL;
  char* testPath = NULL;
  char c;

  while((c = getopt(argc, argv, "p:n:m:")) != -1)
  {
    switch(c)
    {
    case 'm':
      if(!strcmp(optarg, "poll"))
        amode = API_MODE_POLL;
      if(!strcmp(optarg, "writer"))
        amode = API_MODE_WRITER;
      break;
    case 'n':
      testName = optarg;
      break;
    case 'p':
      testPath = optarg;
      break;
    default: break;
    }
  }
  
  if(!testPath) 
  {
    printf("Usage: %s -p<Test Package> -n<Test Name> -m<poll|writer>.\nIf the test name has spaces, place it in quotes.\n", basename(argv[0]));
    return 0;
  }

  FILE* f = fopen(testPath, "r");
  if(!f)
  {
    printf("File %s not found\n", testPath);
    return 0;
  }
  
  WJReader r = WJROpenFILEDocument(f, NULL, 0);
  WJElement e = WJEOpenDocument(r, NULL, NULL, NULL);
  WJRCloseDocument(r);
  fclose(f);
  
  if(!e)
  {
    printf("File %s is invalid JSON.\n", argv[1]);
    WJECloseDocument(e);
    return 0;
  }
  
  WJElement test = NULL;
  while((test = _WJEObject(e, "tests[]", WJE_GET, &test)))
  {
    if(testName && strcmp(testName, WJEString(test, "name", WJE_GET, NULL)))
      continue;
    bool success = true;

    WJElement data = WJEObject(test, "data", WJE_GET);
    PMUS_TEMPLATE* template = mustache_mkIndex(WJEString(test, "template", WJE_GET, NULL), 0);
    char* expected = WJEString(test, "expected", WJE_GET, NULL);

    printf("--Start %s-- \n", WJEString(test, "name", WJE_GET, NULL));
    if(amode == API_MODE_WRITER)
    {
      if(!mustache_generate(
            template,
            &WJEMustacheProvider,
            NULL,
            data,
            writeTest,
            &expected))
      {
        printf("\033[1;31mFAILED! %s\033[0m\n\n", WJEString(test, "desc", WJE_GET, NULL));
        success = false;
      }
    }
    else if(amode == API_MODE_POLL)
    {
      size_t len = 255;
      char buffer[255];
      PMUS_OPERATION* operation = mustache_begin(
        template,
        &WJEMustacheProvider,
        NULL,
        data);
      while(true)
      {
        bool result = mustache_next(operation, buffer, &len);
        if((255 - len) != writeTest(buffer, 255 - len, &expected))
        {
          printf("\033[1;31mFAILED! %s\033[0m\n\n", WJEString(test, "desc", WJE_GET, NULL));
          success = false;
        }
        if(result) break;
      }
    }
    
    if(*expected && success)
      printf("\033[1;31mFAILED! Incomplete template!\033[0m\n\n");
    else puts("\033[1;32mSuccess!\033[0m\n");

    mustache_destroyIndex(template);
  }
}

/*  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
